<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Custom\Bingo;
use App\Custom\Player;

Route::get('/', function () {

    $bingo = new Bingo();
    $player1 = new Player('Keyner');
    $player2 = new Player('Pepito');

    $bingo->callOutNumber(3);
    $bingo->callOutNumber(5);
    $bingo->callOutNumber(6);
    $bingo->callOutNumber(10);
    $bingo->callOutNumber(13);

    $bingo->callOutNumber(16);
    $bingo->callOutNumber(19);
    $bingo->callOutNumber(22);
    $bingo->callOutNumber(23);
    $bingo->callOutNumber(29);

    $bingo->callOutNumber(32);
    $bingo->callOutNumber(33);
    $bingo->callOutNumber(35);
    $bingo->callOutNumber(37);
    $bingo->callOutNumber(44);

    $bingo->callOutNumber(48);
    $bingo->callOutNumber(54);
    $bingo->callOutNumber(55);
    $bingo->callOutNumber(57);
    $bingo->callOutNumber(60);

    $bingo->callOutNumber(61);
    $bingo->callOutNumber(66);
    $bingo->callOutNumber(68);
    $bingo->callOutNumber(69);
    $bingo->callOutNumber(72);

    if ($check1 = $bingo->checkCard($player1->card) || $check2 = $bingo->checkCard($player1->card)) {
        if ($check1) {
            echo $player1->name." is the winner.";
        } else {
            echo $player2->name." is the winner.";
        }
        $bingo->reset();
    } else {
        echo '... again';
    }

    // return view('welcome');
});
