<?php

namespace App\Custom;

class Bingo 
{   

    /**
     * @var Array
     */
    private $numbers = [
        'b' => [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
        'i' => [16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
        'n' => [31,32,33,34,35,36,37,38,39,40,41,42,43,44,45],
        'g' => [46,47,48,49,50,51,52,53,54,55,56,57,58,59,60],
        'o' => [61,62,63,64,65,66,67,68,69,70,71,72,73,74,75],
    ];

    /**
     * @var Array
     */
    public $numbersAlreadyOut;

    /**
     * Class constructor.
     *
     */
    public function __construct()
    {
        $this->numbersAlreadyOut = [];
    }

    /**
     * Reset the Object.
     *
     */
    public function reset()
    {
        parent::__construct();
    }

    /**
     * Generate a Card.
     *
     * @return Array
     */
    public function generateCard()
    {
        return [
            'b' => $this->getRowOfLetter('b', array_rand($this->numbers['b'], 5)),
            'i' => $this->getRowOfLetter('i', array_rand($this->numbers['i'], 5)),
            'n' => $this->getRowOfLetter('n', array_rand($this->numbers['n'], 5)),
            'g' => $this->getRowOfLetter('g', array_rand($this->numbers['g'], 5)),
            'o' => $this->getRowOfLetter('o', array_rand($this->numbers['o'], 5)),
        ];
    }

    /**
     * Store a Call Out Number.
     *
     * @param  Int  $number
     */
    public function callOutNumber(Int $number)
    {
        $this->numbersAlreadyOut[] = $number;
    }

    /**
     * Check a Card.
     *
     * @param  Array  $card
     * 
     * @return Boolean
     */
    public function checkCard(Array $card)
    {
        foreach ($card as $v1) {
            foreach ($v1 as $v2) {
                if (!in_array($v2, $this->numbersAlreadyOut)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Get the Number of an specific Letter and Indexes.
     *
     * @param  String  $letter
     * 
     * @param  Array  $indexes
     * 
     * @return Array
     */
    private function getRowOfLetter(String $letter, Array $indexes) {
        $newArray = [];
        foreach ($indexes as $index) {
            $newArray[] = $this->numbers[$letter][$index];
        }
        return $newArray;
    }

}