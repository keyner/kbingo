<?php

namespace App\Custom;

use App\Custom\Bingo;

class Player
{
    /**
     * @var String
     */
    public $name;

    /**
     * @var Array
     */
    public $card;

    /**
     * Class constructor.
     *
     */
    public function __construct(String $name)
    {
        $this->name = $name;

        $bingo = new Bingo();
        $this->card = $bingo->generateCard();
    }
    
}